#CC : le compilateur à utiliser
CC=gcc

#CFLAGS : les options de compilation  
CFLAGS=$(shell sdl2-config --cflags) -Wall -Wextra -g -DVERBOSE

# les librairies à utiliser 
LIBS=$(shell sdl2-config --libs) -lSDL2_image 

#LDFLAGS : les options d'édition de lien
LDFLAGS=

#lieu où se trouve les sources :
SRC=./src/

#les fichiers objets
OBJ=$(SRC)utils/error.o\
	$(SRC)sprites/sprite.o\
	$(SRC)sprites/spritesManager.o\
	$(SRC)sprites/assets.o\
	$(SRC)world/world.o\
	$(SRC)world/worldFactory.o\
	$(SRC)world/tile.o\
	$(SRC)world/chunk.o\
	$(SRC)serialisation/worldLoader.o\
	$(SRC)serialisation/worldSaver.o\
	$(SRC)list/linkedList.o\
	$(SRC)list/arrayList.o\
	$(SRC)objects/object.o\
	$(SRC)graphics/graphics.o\
	$(SRC)graphics/renderer.o\
	$(SRC)test.o\
	$(SRC)game.o\
	$(SRC)main.o


	


out : $(OBJ)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

$(SRC)%.o: $(SRC)%.c $(SRC)%.h
	$(CC) $(CFLAGS)  $(LDFLAGS) -c $< -o $@

clean:
	rm -rf $(SRC)*.o out

