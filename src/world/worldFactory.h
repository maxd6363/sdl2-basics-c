#ifndef WORLDFACTORY_H_
#define WORLDFACTORY_H_

#include "../error.h"
#include "world.h"


error_t createWorld(world_t *out, int maxSize);
error_t createChunk(chunk_t *out);




#endif