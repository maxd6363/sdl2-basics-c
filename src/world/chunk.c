#include "chunk.h"

void chunkFree(chunk_t *chunk) {
	for (int i = 0; i < CHUNK_SIZE; i++) {
		if (chunk->tiles[i])
			free(chunk->tiles[i]);
	}
	if (chunk->tiles)
		free(chunk->tiles);
	if (chunk->objects)
		free(chunk->objects);
	free(chunk);
}

void chunkPrint(chunk_t chunk) {
	printf("Chunk : %d - %d\n", chunk.x, chunk.y);
	for (int i = 0; i < CHUNK_SIZE; i++) {
		for (int j = 0; j < CHUNK_SIZE; i++) {
            printf("%d",chunk.tiles[i][j]);
        }
        printf("\n");
	}
}
