#include "worldFactory.h"

error_t createWorld(world_t *out, int maxSize) {
	error_t error = OK;
    out->worldSize=maxSize;
	out->chunks = malloc(maxSize * sizeof(chunk_t **));
	if (out->chunks) {
		for (int i = 0; i < maxSize; i++) {
			out->chunks[i] = malloc(maxSize * sizeof(chunk_t *));
			if (out->chunks[i] == NULL) {
				error = ALLOC_ERROR;
			}
			for (int j = 0; j < maxSize; j++) {
                out->chunks[i][j]=malloc(sizeof(chunk_t));
				error = createChunk(out->chunks[i][j]);
			}
		}
	} else {
		error = ALLOC_ERROR;
	}
	return error;
}

error_t createChunk(chunk_t *out) {
	error_t error = OK;
    out->x=rand()%10;
    out->y=rand()%10;
    out->objects=NULL;
	out->tiles = malloc(CHUNK_SIZE * sizeof(tile_t *));
	if (out->tiles) {
		for (int i = 0; i < CHUNK_SIZE; i++) {
			out->tiles[i] = malloc(CHUNK_SIZE * sizeof(tile_t *));
			if (out->tiles[i] == NULL)
				error = ALLOC_ERROR;
		}
	} else {
		error = ALLOC_ERROR;
	}
	return error;
}
