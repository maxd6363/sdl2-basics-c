#include "world.h"





void worldFree(world_t *world){
    for (int i = 0; i < world->worldSize; i++) {
		for (int j = 0; j < world->worldSize; j++) {
            chunkFree(world->chunks[i][j]);
        }
        free(world->chunks[i]);
	}
    free(world->chunks);
    world->worldSize=0;
}




void worldPrint(world_t world){
    printf("World - Debug\n");
	for (int i = 0; i < world.worldSize; i++) {
		for (int j = 0; j < world.worldSize; j++) {
            printf("%d ",world.chunks[i][j]!=NULL);
        }
        printf("\n");
	}
}

