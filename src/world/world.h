#ifndef WORLD_H_
#define WORLD_H_

#include "chunk.h"

typedef struct {
    chunk_t ***chunks;    
    int worldSize;
}world_t;



void worldFree(world_t *world);
void worldPrint(world_t world);





#endif