#ifndef CHUNK_H_
#define CHUNK_H_

#include "../objects/object.h"
#include "tile.h"

#define CHUNK_SIZE 16


typedef struct{
    object_t **objects;
    tile_t **tiles;
    int x;
    int y;  
}chunk_t;



void chunkFree(chunk_t *chunk);
void chunkPrint(chunk_t chunk);



#endif