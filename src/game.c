#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/graphics.h"
#include "graphics/renderer.h"
#include "serialisation/worldLoader.h"
#include "sprites/assets.h"
#include "sprites/sprite.h"
#include "utils/bool.h"
#include "world/world.h"


#define MS_FRAME 12

config_t gameConfig;

void gameSetup(config_t config) {
	gameConfig = config;
}

void gameStart(SDL_Window *window) {
	srand(time(NULL));
	bool running = true;
	int characterSpeed = 5;
	Uint8 *keystates;
	SDL_Event event;
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	int windowWidth,windowHeight;
	
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);

	world_t world;
	worldLoad(&world, "data/worlds/world1.dat");

	rendererInit(renderer);


	//sprite_t cat[10];
	//asset_t *catAssets = assetsLoad(4, PATH_TO_SPRITES"catTEST", "cat", PNG_EXTENSION, renderer);

	/*
	for (int i = 0; i < 10; i++) {
		spriteInit(catAssets,2, 60, rand() % windowWidth, rand() % windowHeight, &cat[i]);
		spriteDebug(&cat[i]);
	}
	*/

	while (running) {
		gameRenderGame(window);
		renderWorld(&world,renderer);
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					running = false;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					gameRenderGame(window);
					break;
				}
				break;
			case SDL_QUIT:
				running = false;
				break;
			}
		}

		keystates = SDL_GetKeyboardState(NULL);
		/*
		for (int i = 0; i < 10; i++) {
			spriteMoveRandom(&cat[i]);
			if (keystates[SDL_SCANCODE_LEFT])
				cat[i].rect.x -= characterSpeed;
			else if (keystates[SDL_SCANCODE_RIGHT])
				cat[i].rect.x += characterSpeed;
			else if (keystates[SDL_SCANCODE_UP])
				cat[i].rect.y -= characterSpeed;
			else if (keystates[SDL_SCANCODE_DOWN])
				cat[i].rect.y += characterSpeed;
			else if (keystates[SDL_SCANCODE_DELETE]){
				spriteDelete(&cat[(SDL_GetTicks()/200)%10]);
			}
			spriteRender(&cat[i], renderer);
		}
*/
		SDL_RenderPresent(renderer);
		SDL_Delay(MS_FRAME);
	}

	gameEnd(window);
	for (int i = 0; i < 10; i++) {
		//spriteFree(&cat[i]);
	}
	worldFree(&world);
	rendererDestroy();
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
}

void gameRenderGame(SDL_Window *window) {
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer);
	//int windowWidth;
	//int windowHeight;

	//SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	//printf("Window rendering : %d %d\n", windowWidth, windowHeight);
}
