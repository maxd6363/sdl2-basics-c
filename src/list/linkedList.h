#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "../utils/error.h"

typedef int list_data_t;

typedef struct node {
	struct node *next;
	list_data_t data;
} node_t, *list_t;

error_t nodeInit(node_t **out, list_data_t data);

error_t nodeFree(node_t *node);

list_t listInit(void);

list_t listForeach(list_t list, void (*func)(list_data_t));

int listLenght(list_t list);

error_t listFree(list_t *list);

error_t listPrint(list_t list);

error_t listAdd(list_t *prec, list_data_t data);

error_t listAddChain(list_t *prec, node_t *maillon);

error_t listRemove(node_t **prec);

#endif