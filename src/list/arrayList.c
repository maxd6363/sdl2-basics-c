#include "arrayList.h"

#include <stdlib.h>

error_t arrayListInit(int size, array_list_t *out) {
	out->maxSize = size;
	out->numberElements = 0;
	out->data = malloc(size * sizeof(array_list_data_type));
	return out->data == NULL ? ALLOC_ERROR : OK;
}

void arrayListFree(array_list_t *list) {
	list->maxSize = 0;
	list->numberElements = 0;
	free(list->data);
	list->data = NULL;
}

error_t arrayListAdd(array_list_t *list, array_list_data_type data) {
	error_t error=ERROR;
    if (list->numberElements < list->maxSize) {
		list->data[list->numberElements] = data;
		(list->numberElements)++;
        error=OK;
	}
    return error;
}

array_list_data_type arrayListGet(array_list_t list, int index) {
    return list.data[index];
}

array_list_data_type arrayListRemove(array_list_t *list, int index) {
	array_list_data_type data;
    if (list->numberElements > 0) {
		data = list->data[list->numberElements - 1];
		(list->numberElements)--;
	}
    return data;
}
