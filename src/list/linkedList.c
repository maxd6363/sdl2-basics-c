#include "linkedList.h"

#include <stdio.h>
#include <stdlib.h>

error_t nodeInit(node_t **out, list_data_t data) {
	error_t error = OK;
	if (out == NULL) {
		error = NULL_POINTER;
	} else {
		*out = (node_t *)malloc(sizeof(node_t));
		if (out != NULL) {
			(*out)->data = data;
		} else {
			error = ALLOC_ERROR;
			out = NULL;
		}
	}
	return error;
}

error_t nodeFree(node_t *node) {
	if (node != NULL) {
		free(node);
	}
	return OK;
}

list_t listInit(void) {
	return NULL;
}

list_t listForeach(list_t list, void (*func)(list_data_t)) {
	node_t *tmp = list;
	while (tmp != NULL) {
		func(tmp->data);
		tmp = tmp->next;
	}
	return OK;
}

int listLenght(list_t list) {
	node_t *tmp = list;
	int size = 0;
	while (tmp != NULL) {
		size++;
		tmp = tmp->next;
	}
	return size;
}

error_t listFree(list_t *list) {
	while (*list != NULL) {
		listRemove(list);
	}
	*list = listInit();
	return OK;
}

error_t listPrint(list_t list) {
	printf("NOT IMPLEMENTED \n");
	return OK;
}

error_t listAdd(list_t *prec, list_data_t data) {
	error_t error;
	node_t *tmp = NULL;

	error = nodeInit(&tmp, data);
	if (error == OK) {
		listAddChain(prec, tmp);
	}
	return error;
}

error_t listAddChain(list_t *prec, node_t *maillon) {
	maillon->next = *prec;
	*prec = maillon;
	return OK;
}

error_t listRemove(node_t **prec) {
	node_t *toFree = *prec;
	*prec = (*prec)->next;
	nodeFree(toFree);
	return OK;
}
