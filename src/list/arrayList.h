#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include "../utils/error.h"

typedef int array_list_data_type;

typedef struct {
	int maxSize;
	int numberElements;
	array_list_data_type *data;
} array_list_t;

error_t arrayListInit(int size, array_list_t *out);
void arrayListFree(array_list_t *list);
error_t arrayListAdd(array_list_t *list, array_list_data_type data);
array_list_data_type arrayListGet(array_list_t list, int index);
array_list_data_type arrayListRemove(array_list_t *list, int index);

#endif