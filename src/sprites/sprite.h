#ifndef SPRITE_H
#define SPRITE_H


#include "../utils/string.h"
#include "../utils/error.h"
#include "../utils/direction.h"
#include "assets.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>



typedef struct {
    int animationSpeed;
    float scale;
   	SDL_Rect rect;
    asset_t *asset;

}sprite_t;



error_t spriteInit(asset_t *asset, float scale, int animationSpeed, int x, int y, sprite_t *out);
void spriteFree(sprite_t *sprite);
void spriteDelete(sprite_t *sprite);
void spriteRender(sprite_t *sprite,SDL_Renderer *renderer);
void spriteDebug(sprite_t *sprite);
void spriteMoveRandom(sprite_t *sprite);

 
#endif