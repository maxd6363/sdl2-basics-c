#ifndef ASSETS_H
#define ASSETS_H

#include "../error.h"
#include "../utils/string.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#define PATH_TO_SPRITES "./data/sprites/"
#define PNG_EXTENSION "png"


typedef struct {
	int sharedBy;
	int numberOfParts;

	char path[MAX_STRING_SIZE];
	char filename[MAX_STRING_SIZE];
	char extention[MAX_STRING_SIZE];

	SDL_Surface **surfaces;
	SDL_Texture **textures;

} asset_t;

asset_t *assetsLoad(int numberOfParts, const char path[], const char filename[], const char extention[], SDL_Renderer *renderer);
void assetsFree(asset_t *asset);

#endif