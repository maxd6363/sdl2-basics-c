#include "sprite.h"

#include <string.h>

error_t spriteInit(asset_t *asset, float scale, int animationSpeed, int x, int y, sprite_t *out) {
	error_t error = OK;
	out->animationSpeed = animationSpeed;
	out->scale = scale;
	out->rect.x = x;
	out->rect.y = y;
	out->asset = asset;
	if (asset != NULL) {
		out->rect.w = out->asset->surfaces[0]->w * scale;
		out->rect.h = out->asset->surfaces[0]->h * scale;
		(asset->sharedBy)++;
	}
	return error;
}

void spriteFree(sprite_t *sprite) {
	if (sprite->asset != NULL) {
		(sprite->asset->sharedBy)--;
		assetsFree(sprite->asset);
		sprite->asset = NULL;
	}
	spriteDelete(sprite);
}

void spriteDelete(sprite_t *sprite) {
	sprite->animationSpeed = 0;
	sprite->scale = 0;
	sprite->rect = (SDL_Rect){0, 0, 0, 0};
}

void spriteRender(sprite_t *sprite, SDL_Renderer *renderer) {
	if (sprite != NULL && sprite->asset != NULL && sprite->rect.w != 0 && sprite->rect.h != 0 && sprite->scale !=0)
		SDL_RenderCopy(renderer, sprite->asset->textures[(SDL_GetTicks() / sprite->animationSpeed) % sprite->asset->numberOfParts], NULL, &(sprite->rect));
}

void spriteDebug(sprite_t *sprite) {
	printf("Sprite %p\n", sprite);
	printf("\tNumber of parts : %d\n", sprite->asset->numberOfParts);
	printf("\tAnimation speed : %d\n", sprite->animationSpeed);
	printf("\tScale           : %f\n", sprite->scale);
	printf("\tFiles           : %s/%s_x.%s\n", sprite->asset->path, sprite->asset->filename, sprite->asset->extention);
	printf("\tSize            : %d x %d\n", sprite->rect.w, sprite->rect.h);
	printf("\tPosition        : %d x %d\n", sprite->rect.x, sprite->rect.y);
	printf("\tAssets location : %p\n", sprite->asset);
}

void spriteMoveRandom(sprite_t *sprite) {
	direction_t dir = rand() % NUMBER_OF_DIRECTION;
	int speed = 1;
	switch (dir) {
	case UP:
		sprite->rect.y -= speed;
		break;
	case DOWN:
		sprite->rect.y += speed;
		break;
	case LEFT:
		sprite->rect.x -= speed;
		break;
	case RIGHT:
		sprite->rect.x += speed;
		break;
	}
}