#include "assets.h"

#include "../utils/error.h"

asset_t *assetsLoad(int numberOfParts, const char path[], const char filename[], const char extention[], SDL_Renderer *renderer) {
	asset_t *asset = malloc(sizeof(asset_t));
	char tmp[MAX_STRING_SIZE];

	if (asset != NULL) {
		strcpy(asset->path, path);
		strcpy(asset->filename, filename);
		strcpy(asset->extention, extention);
		asset->sharedBy = 0;
		asset->numberOfParts = numberOfParts;
		asset->surfaces = malloc(numberOfParts * sizeof(SDL_Surface *));
		if (asset->surfaces != NULL) {
			asset->textures = malloc(numberOfParts * sizeof(SDL_Texture *));
			if (asset->textures != NULL) {
				for (int i = 0; i < numberOfParts; i++) {
					sprintf(tmp, "%s/%s_%d.%s", path, filename, i, extention);
					asset->surfaces[i] = IMG_Load(tmp);
					asset->textures[i] = SDL_CreateTextureFromSurface(renderer, asset->surfaces[i]);
					if(asset->surfaces[i] == NULL || asset->textures[i]==NULL)
						logError(ASSETS_LOADING_FAILED);
				}
			}
		}
	}
	if(asset == NULL || (asset != NULL && asset->surfaces==NULL)|| (asset != NULL && asset->textures==NULL))
		logError(ASSETS_LOADING_FAILED);
	return asset;
}

void assetsFree(asset_t *asset) {
	if (asset != NULL) {
		for (int i = 0; i < asset->numberOfParts; i++) {
			SDL_DestroyTexture(asset->textures[i]);
			SDL_FreeSurface(asset->surfaces[i]);
		}
		free(asset->surfaces);
		free(asset->textures);
		asset->surfaces = NULL;
		asset->textures = NULL;
		asset->numberOfParts = 0;
		if(asset->sharedBy==0)
			free(asset);
	}
}
