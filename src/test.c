#include "test.h"

#include "game.h"
#include "graphics/graphics.h"
#include "serialisation/worldLoader.h"
#include "serialisation/worldSaver.h"
#include "world/worldFactory.h"

#include <SDL2/SDL.h>
#include <stdio.h>

void testGraphics(void) {
	SDL_Window *window;
	SDL_Renderer *renderer;

	gcsInit();

	gcsNewWindow("My Game", 1280, 720, &window);
	gcsNewRenderer(window, &renderer);

	gameStart(window);

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();
}

void testWorld(void) {

	world_t world;
	createWorld(&world, 5);

	worldPrint(world);
	worldFree(&world);
}

void testSerialisation(void) {
	world_t world;
	world_t world2;

	createWorld(&world, 5);

	for (int i = 0; i < world.worldSize; i++) {
		for (int j = 0; j < world.worldSize; j++) {
			world.chunks[i][j]->x = i;
			world.chunks[i][j]->y = j;
			for (int ic = 0; ic < CHUNK_SIZE; ic++) {
				for (int jc = 0; jc < CHUNK_SIZE; jc++) {
					world.chunks[i][j]->tiles[ic][jc] = rand()%2;
				}
			}
		}
	}

	worldSave(&world, "data/worlds/world1.dat");

	worldLoad(&world2, "data/worlds/world1.dat");
	
	
	
	worldSave(&world2, "data/worlds/world2.dat");
	worldPrint(world2);

	worldFree(&world);
	worldFree(&world2);
	
}
