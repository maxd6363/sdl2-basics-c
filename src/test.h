#ifndef TEST_H_
#define TEST_H_


void testGraphics(void);
void testWorld(void);
void testSerialisation(void);

#endif