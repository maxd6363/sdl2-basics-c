#include "worldLoader.h"

#include <stdlib.h>

#include "../utils/app.h"
#include "../world/worldFactory.h"

error_t worldLoad(world_t *out, const char *filename) {
	error_t error = OK;
	FILE *flot;
	int worldSize = 0;
	int appVersion;
	tile_t tile;
	int x, y;

	flot = fopen(filename, "r");
	if (flot == NULL) {
		error = FILE_NOT_FOUND;
	} else {
		fscanf(flot, "%d", &appVersion);
		if (appVersion != APP_VERSION) {
			error = WRONG_APP_VERSION;
		} else {
			fscanf(flot, "%d", &worldSize);
			if (createWorld(out, worldSize) != OK) {
				error = ERROR;
			} else {
				for (int i = 0; i < worldSize; i++) {
					for (int j = 0; j < worldSize; j++) {
						fscanf(flot, "%d %d", &x, &y);
						out->chunks[i][j]->x = x;
						out->chunks[i][j]->y = y;
						for (int ic = 0; ic < CHUNK_SIZE; ic++) {
							for (int jc = 0; jc < CHUNK_SIZE; jc++) {
                                fscanf(flot, "%d", (int*)&tile);
								out->chunks[i][j]->tiles[ic][jc] = tile;
							}
						}
					}
				}
			}
		}
        fclose(flot);
	}
	return error;
}