#include "worldSaver.h"

#include <stdlib.h>

#include "../utils/app.h"
#include "../world/worldFactory.h"

error_t worldSave(world_t *world, const char *filename) {

	error_t error = OK;
	FILE *flot;

	if (world == NULL) {
		error = NULL_POINTER;
	} else {
		flot = fopen(filename, "w");
		if (flot == NULL) {
			error = FILE_NOT_FOUND;
		} else {
			fprintf(flot, "%d\n", APP_VERSION);
			fprintf(flot, "%d\n", world->worldSize);

			for (int i = 0; i < world->worldSize; i++) {
				for (int j = 0; j < world->worldSize; j++) {
					fprintf(flot, "\n\n%d %d\n", world->chunks[i][j]->x, world->chunks[i][j]->y);
					for (int ic = 0; ic < CHUNK_SIZE; ic++) {
						for (int jc = 0; jc < CHUNK_SIZE; jc++) {
							fprintf(flot, "%d ", world->chunks[i][j]->tiles[ic][jc]);
						}
                        fprintf(flot,"\n");
					}
				}
			}

			fclose(flot);
		}
	}
	return error;
}