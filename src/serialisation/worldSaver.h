#ifndef WORLDSAVER_H_
#define WORLDSAVER_H_

#include "../error.h"
#include "../world/world.h"

error_t worldSave(world_t *world, const char *filename);

#endif