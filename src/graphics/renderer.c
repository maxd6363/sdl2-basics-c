#include "renderer.h"

struct {
	asset_t *paths[2];

} world_asset;

enum { DEFAULT } path_orientation;

void rendererInit(SDL_Renderer *renderer) {
	world_asset.paths[0] = NULL;
	world_asset.paths[1] = assetsLoad(1, PATH_TO_SPRITES "path", "path", PNG_EXTENSION, renderer);
}

void renderWorld(world_t *world, SDL_Renderer *renderer) {
	if (world) {
		for (int i = 0; i < world->worldSize; i++) {
			for (int j = 0; j < world->worldSize; j++) {
				renderChunk(world->chunks[i][j], renderer);
			}
		}
	}
}

void renderChunk(chunk_t *chunk, SDL_Renderer *renderer) {
	for (int ic = 0; ic < CHUNK_SIZE; ic++) {
		for (int jc = 0; jc < CHUNK_SIZE; jc++) {
			renderTile(chunk->tiles[ic][jc], chunk->x*CHUNK_SIZE*TILE_SIZE_PX + ic * TILE_SIZE_PX, chunk->y*CHUNK_SIZE*TILE_SIZE_PX + jc * TILE_SIZE_PX, renderer);
		}
	}
}

void renderTile(tile_t tile, int x, int y, SDL_Renderer *renderer) {
	if (world_asset.paths[tile]) {
		SDL_Rect rect = {x, y, TILE_SIZE_PX, TILE_SIZE_PX};
		SDL_RenderCopy(renderer, world_asset.paths[tile]->textures[0], NULL, &rect);
	}
}

void rendererDestroy(void) {
	assetsFree(world_asset.paths[1]);
}
