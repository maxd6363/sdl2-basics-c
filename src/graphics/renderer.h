#ifndef RENDERER_H_
#define RENDERER_H_

#include "../world/chunk.h"
#include "../world/tile.h"
#include "../world/world.h"

#include <SDL2/SDL.h>

#define TILE_SIZE_PX 48



void rendererInit(SDL_Renderer *renderer);

void renderWorld(world_t *world, SDL_Renderer *renderer);
void renderChunk(chunk_t *chunk, SDL_Renderer *renderer);
void renderTile(tile_t tile, int x, int y, SDL_Renderer *renderer);

void rendererDestroy(void);


#endif